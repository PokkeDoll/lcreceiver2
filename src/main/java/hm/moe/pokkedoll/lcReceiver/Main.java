package hm.moe.pokkedoll.lcReceiver;

import com.github.ucchyocean.lc.LunaChat;
import com.github.ucchyocean.lc.LunaChatAPI;
import com.github.ucchyocean.lc.channel.ChannelPlayer;
import com.github.ucchyocean.lc.event.LunaChatChannelMessageEvent;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class Main extends JavaPlugin implements Listener, PluginMessageListener {

    private LunaChatAPI api;

    @Override
    public void onEnable() {
        api = LunaChat.getInstance().getLunaChatAPI();
        Bukkit.getPluginManager().registerEvents(this, this);
        getServer().getMessenger().registerIncomingPluginChannel(this, "lcReceiver", this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "lcReceiver");
    }

    @EventHandler
    public void onChat(LunaChatChannelMessageEvent e) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        if(e.getChannel().isGlobalChannel()) {
            out.writeUTF("global");
            out.writeUTF("[From: "+getServer().getServerName()+"] "+e.getMessage());
            out.writeUTF(getServer().getServerName());
        } else if(e.getChannelName().equals("UNEI")) {
            out.writeUTF("unei");
            out.writeUTF("[From: "+getServer().getServerName()+"] "+e.getMessage());
            out.writeUTF(getServer().getServerName());
        } else {
            out.writeUTF(e.getChannelName());
            out.writeUTF("[From: "+getServer().getServerName()+"] "+e.getMessage());
            out.writeUTF(getServer().getServerName());
        }
        e.getPlayer().getPlayer().sendPluginMessage(this, "lcReceiver", out.toByteArray());
    }

    /**
     * channel: lcReceiverのみ受信
     * in:
     * global => グローバルチャット
     * unei => 運営チャット
     * ()* => 任意のチャンネルチャット
     *
     * @param channel
     * @param player
     * @param message
     */
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if(!channel.equals("lcReceiver")) return;
        try {
            ByteArrayDataInput in = ByteStreams.newDataInput(message);
            String subChannel = in.readUTF();
            if(subChannel.equalsIgnoreCase("global")) {
                String msg = in.readUTF();
                String server = in.readUTF();
                if(!server.equals(getServer().getName())) {
                    Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(msg));
                }
            } else if(subChannel.equalsIgnoreCase("unei")) {
                String msg = in.readUTF();
                String server = in.readUTF();
                if(!server.equals(getServer().getName())) {
                    api.getChannel("UNEI").getMembers()
                            .stream()
                            .filter(ChannelPlayer::isOnline)
                            .forEach(p -> p.sendMessage(msg));
                }
            } else if(api.isExistChannel(subChannel)) {
                    String msg = in.readUTF();
                    String server = in.readUTF();
                    if(!server.equals(getServer().getName())) {
                        api.getChannel(subChannel).getMembers()
                                .stream()
                                .filter(ChannelPlayer::isOnline)
                                .forEach(p -> p.sendMessage(msg));
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
